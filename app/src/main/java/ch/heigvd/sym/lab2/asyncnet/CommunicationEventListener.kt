package ch.heigvd.sym.lab2.asyncnet

interface CommunicationEventListener {
    fun handleServerResponse(response :String)
}
